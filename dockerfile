FROM debian:bullseye

ARG BUILD_DATE
ARG VCS_REF
ARG NGINX_VERSION

LABEL MAINTAINER="max@maxtpower.com" \
org.label-schema.schema-version="1.0" \
org.label-schema.build-date=${BUILD_DATE} \
org.label-schema.name="maxtpower/twitch-rtmp-proxy" \
org.label-schema.description="Twitch RTMP Proxy Service" \
org.label-schema.vcs-url="https://gitlab.com/m1xp3r/twitch-rtmp-proxy" \
org.label-schema.vcs-ref=${VCS_REF} \
org.label-schema.vendor="maxtpower"

RUN export DEBIAN_FRONTEND=noninteractive \
&& apt-get update -qq \
&& apt-get install -yqq build-essential libpcre3 libpcre3-dev zlib1g-dev libssl-dev wget unzip \
&& wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz \
&& wget https://github.com/sergey-dryabzhinsky/nginx-rtmp-module/archive/refs/heads/dev.zip \
&& tar -zxvf nginx-${NGINX_VERSION}.tar.gz \
&& unzip dev.zip \
&& cd nginx-${NGINX_VERSION} \
&& ./configure --with-http_ssl_module --add-module=../nginx-rtmp-module-dev \
&& make \
&& make install \
&& cd .. \
&& rm -rf nginx-${NGINX_VERSION}.tar.gz \
&& rm -rf nginx-${NGINX_VERSION} \
&& rm -rf dev.zip \
&& mkdir -p /opt/rtmp && cp nginx-rtmp-module-dev/stat.xsl /opt/rtmp/ \
&& rm -rf nginx-rtmp-module-dev \
&& apt-get purge -yqq build-essential wget unzip \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/* \
&& unset DEBIAN_FRONTEND

COPY /config/nginx.conf /usr/local/nginx/conf/nginx.conf

EXPOSE 8080 1935

CMD ["/usr/local/nginx/sbin/nginx","-g","daemon off;"]
