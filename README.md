# Twitch RTMP Proxy Service

Designed primarily for users who wish to stream consoles to OBS via built in Twitch streaming capabilities. There are
also many other practice uses.

This image is simply an RTMP Proxy built using Nginx and [nginx-rtmp-module](http://nginx-rtmp.blogspot.com/)

The Twitch proxy acts as a man in the middle to your Twitch stream, allowing you to ingest it into any RTMP broadcast
platform.

The RTMP proxy listens on port 1935 and the stats server on port 8080.

### Notes when using Consoles built for Twitch

- Before you will be able to accept stream data, you will need to setup a local DNS redirect for
  the [Twitch ingest servers](https://stream.twitch.tv/ingests/). The redirect should point to the IP address of your
  docker host.
- Your console/streaming device will need to use this local DNS server, instead of any defaults.

## Usage

- `docker run -d --rm --name livestream -p 1935:1935 -p 8080:8080 maxtpower/twitch-rtmp-proxy`
